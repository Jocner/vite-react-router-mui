import React from "react";
import ReactDOM from "react-dom/client";
import { WrappedApp } from "./App";
import Footer from "./components/Footer";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <WrappedApp />
   
    {/* <br></br> */}
    {/* <Footer/> */}
  </React.StrictMode>
);

import React from 'react'

const Footer = () => {
  return (
   <body>
      <div>
        <footer className="footer">
          <p className="footer-by">
            A project by{" "}
            Jocner Patiño Buznego
          </p>
        </footer>
      </div>
    </body>
  )
}

export default Footer;
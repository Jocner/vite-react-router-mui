import React from "react";
import Navbar from "../Navbar";
// import PropTypes from 'prop-types';
import { m } from 'framer-motion';
// @mui
import Stack from '@mui/material/Stack';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Carousel from "react-material-ui-carousel";
import ReactPlayer from 'react-player';
import bg1 from "../../image/grupo.jpg";
import bg2 from "../../image/image1.jpg";
import bg3 from "../../image/image3.jpg";
import bg4 from "../../image/image12.jpg";
import bg5 from "../../image/image5.jpg";
import video from '../../image/video1.mp4'
import "./home.scss";

// const bgImages = [bg1, bg2];
const bgImages = [bg1, bg2, bg3, bg4, bg5];


function Home() {
  return (
    <body>
      <Navbar />

      <div className="home__container">
        {/* <Carousel
          autoPlay={true}
          indicators={false}
          className="home__carousel"
          navButtonsAlwaysVisible={false}
          navButtonsAlwaysInvisible={false}
        >
          {bgImages.map((item, i) => (
            <img
              key={i}
              src={item}
              alt={`Amazon Background ${i}`}
              className="home__image"
            />
          ))}
        </Carousel> */}
        <ReactPlayer
          url={video}
          width='100%'
          height='100%'
          controls={false}
          playing={true}
          // loop
          className="react-player"
        />
       
      </div>

      <Container className="contenedor">


      <ReactPlayer
          url={video}
          width='100%'
          height='100%'
          controls={false}
          playing={true}
          // loop
          className="react-player"
        />

        

      </Container>

      {/* <Box
      sx={{
        height: { md: 560 },
        py: { xs: 10, md: 0 },
        overflow: 'hidden',
        position: 'relative',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundImage:
          'url(/assets/background/overlay_1.svg), url(/assets/images/about/hero.jpg)',
      }}
    > */}
      {/* <Container component={MotionContainer}> */}
      {/* <Container >
        <Box
          sx={{
            bottom: { md: 80 },
            position: { md: 'absolute' },
            textAlign: {
              xs: 'center',
              md: 'unset',
            },
          }}
        > */}
          {/* <TextAnimate text="Who" variants={varFade().inRight} sx={{ color: 'primary.main' }} /> */}

          {/* <br />

          <Stack spacing={2} display="inline-flex" direction="row" sx={{ color: 'common.white' }}> */}
            {/* <TextAnimate text="we" />
            <TextAnimate text="are?" /> */}
          {/* </Stack> */}

          {/* <m.div variants={varFade().inRight}> */}
            {/* <Typography
              variant="h4"
              sx={{
                mt: 3,
                color: 'common.white',
                fontWeight: 'fontWeightSemiBold',
              }}
            >
              Let&apos;s work together and
              <br /> make awesome site easily
            </Typography> */}
          {/* </m.div> */}
        {/* </Box>
      </Container>
    </Box> */}
  
      {/* <footer className="footer">
        <p className="footer-by">
          A project by{" "}
          Jocner Patiño Buznego
        </p>
      </footer> */}
    </body>
  );
}

export default Home;
